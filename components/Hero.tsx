import Image from 'next/image'

const Hero = () => {
  return (
    <section className='max-container padding-container flex flex-col gap-20 py-10 pb-32 md:gap-28 lg:py-20 xl:flex-row border-2 border-red-500'>
      <div className='hero-map'/>

      {/* LEFT */}
      <div className='relative z-20 flex flex-1 flex-col xl:w-1/2'>
        <Image 
          src="/camp.svg" 
          alt="camp" 
          width={50} 
          height={50} 
          className='absolute left-[-5px] top-[-30px] w-10 lg:w-[50px]'
        />
        <h1 className='bold-52 lg:bold-88'>Soberanía Patriota C.A.</h1>
        <p className='regular-16 mt-6 text-gray-30 xl:max-w-[520px]'>
           Empresa Socialista de Servicios y Suministro de Alimentos Soberanía Patriota C.A, fue creada en materia de alimentación, basándose en el Segundo Objetivo del Plan de la Patria con el objetivo de suministrar la mayor suma de alimentos necesarios de la cesta básica a la población del estado Monagas. De igual manera, la empresa desarrolla como actividad principal una serie de atenciones a comunidades, organismos e instituciones con el fin de satisfacer la demanda de alimentos existentes.
        </p>
        <div className='my-11 flex flex-wrap gap-5'>
          <div className='flex items-center gap-2'>
            {Array(5).fill(1).map((_, i) => (
              <Image 
                key={i}
                src="/star.svg" 
                alt="star" 
                width={24} 
                height={24} 
              />
            ))}
          </div>
          <p className='bold-16 lg:bold-20 text-blue-70'>
            198k
            <span className='text-gray-30'> Excelente Puntación</span>
          </p>
        </div>
      </div>
    </section>
  )
}

export default Hero